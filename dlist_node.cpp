#include "dlist_node.h"

/*
class DListNode
{
    public: 
        DListNode(const double inputData, const DListNode * prev=NULL, const DListNode*next=NULL);
    
        double data() const;
        bool setData(const double inputData);
        
        const DListNode * prev() const;
        const DListNode * next() const;
        bool linkPrev(  const DListNode * PrevNode);
        bool linkNext(  const DListNode * NextNode);
        void show() const;

    private: 
        double _Data;
        const DListNode * _Prev;
        const DListNode * _Next;


};


*/

using std::cout;
using std::endl;
// using DListNode::value_type;

DListNode::DListNode(const value_type inputData,  DListNode * prev,  DListNode*next) // @suppress("Class members should be properly initialized")
{
    this->setData(inputData);
    this->linkNext(next); 
    this->linkPrev(prev);
    // DListNode * _Next = NULL;

}

DListNode::value_type DListNode::data() const
{
    return this->_Data;
}

bool DListNode::setData(const value_type inputData)
{
    this->_Data = inputData;
    return true;
}


 DListNode * DListNode::prev() const
{
     return this->_Prev;
}

 DListNode * DListNode::next() const
{
    return (this->_Next);
}


void DListNode::linkPrev( DListNode * PrevNode)
{
    this->_Prev = PrevNode;
}

void DListNode::linkNext( DListNode * NextNode)
{
    this->_Next = NextNode;
}

void DListNode::show() const
{
    cout << "data: " << this->data() << endl;
    cout << "prev_addr: " << this->prev() << endl;
    cout << "next_addr: " << this->next() << endl;
    if (this->_Prev != NULL)
    {
        cout << "prev_data: " << this->prev()->data() << endl;
    }

    if (this->_Next != NULL)
    {
        cout << "next_data: " << this->next()->data()<< endl;
    }
}
