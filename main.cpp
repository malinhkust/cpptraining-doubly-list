#include "dlist_node.h"
#include "double_list.h"
#include <iostream>

using std::string ;
using std::cout;
using std::endl;

void po(string title, double data)
{
    cout << title << data << endl;
};

int main()
{
    DListNode N1('s');
    DListNode N2(2.1);
    DListNode N3(3.1);

    // N1.show();
    // N2.show();
    // N3.show();

    cout << " ----1---- "<<endl;

    N2.linkNext(&N3);
    N2.linkPrev(&N1);

    // N2.show();

    Double_List DL;
    for (int i = 0; i < 10; i++)
    {
        DL.append(double(i));
    }

    DL.show();

    
    po("len: ", DL.length());
    po("at: ", DL.at(2));
    po("at: ", DL.at(-1));
    // for (int i = 0; i<=3;i++)
    // {
    //     cout <<i<<endl;
    // }


    return 1;
}