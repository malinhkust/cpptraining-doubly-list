#ifndef __DLIST_H__
#define __DLIST_H__

#include <stddef.h>
#include <iostream>

class DListNode
{
    public: 
        typedef double value_type;

        
        DListNode(const value_type inputData,  DListNode * prev=NULL,  DListNode*next=NULL);
    
        value_type data() const;
        bool setData(const value_type inputData);
        
        DListNode * prev() const;
        DListNode * next() const;
        void linkPrev(   DListNode * PrevNode);
        void linkNext(   DListNode * NextNode);
        void show() const;

    private: 
        value_type _Data;
        DListNode * _Prev;
        DListNode * _Next;


};



// class DoubleList
// {
//     public:
//         DoubleList(const double * data, const int dataLen );
//         DoubleList();


//     private: 

// }



#endif