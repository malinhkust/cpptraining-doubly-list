#include "double_list.h"


using std::cout;
using std::endl;
/*
#ifndef __DOUBLE_LIST_H__
#define __DOUBLE_LIST_H__

#include "dlist_node.h"

class Double_List
{
    public: 
        Double_List();
        Double_List(double*data, int dataNum);

        void insert(int index, double data);
        void append(double data);
        
        

    private: 
        DListNode _create_node(double data);

        DListNode * _tail;
        DListNode * _head;
};



#endif
*/

Double_List::Double_List()
{
    this->_tail = NULL;
    this->_head = NULL;
}

DListNode * Double_List::_createNode(double data)
{
    DListNode * NodePointer = new DListNode(data);
    
    return NodePointer;
}

bool Double_List::isEmpty() const
{
    return (this->_head == NULL);
}

bool Double_List::_isIndexOutOfRange(int queryIndex) const throw(std::invalid_argument, int)
{
    // assert (queryIndex > 0);
    if (queryIndex < 0)
        { throw( std::invalid_argument(" query index < 0" ) ); }
        
    
    int CurrentLength = this->length();
    bool OutOfRange = queryIndex >= CurrentLength;
    if (OutOfRange)
        {  throw(CurrentLength); }
       
    return (OutOfRange);
}

double Double_List::at(int index) const
{
    // Out-Of-Range check
    try
    { 
        if (!this->_isIndexOutOfRange(index))
        {
            cout << "jump!"<<endl;
            DListNode * cursor = this->_jumpTo(index);
            return cursor->data();
        }
        else
        {
            throw("_isIndexOutOfRange failed undefined!");
        }
        
    }

    catch (int excep_length)
    {
        cout << "EXCEPTION: Index Out of Range"
             << " index : " << index
             << " length: " << excep_length <<endl;
        std::terminate();
        // std::terminate();
    }

    catch(std::invalid_argument &e)
    {
        cout << "EXCEPTION: "<< e.what() << endl;
        std::terminate();
    }
    catch(...)
    {
        cout << "any other exception!"<<endl;
        std::terminate();
    }
    
}



DListNode * Double_List::_jumpTo(int index) const 
{
    DListNode * cursor = this->_head;

    for (int i = 0; i < index; i++)
    {
        cursor = cursor->next();
    }
    return cursor;
}


double Double_List::length() const
{
    DListNode *cursor = this->_head;
    int length = 0;

    while ( cursor != NULL)
    {
        cursor = cursor->next();
        ++length;
    }
    return length;
}

    // double Double_List::insert(int index) const
    // {
    //     const DListNode * insert_prev = this->_head;
    //     const DListNode *insert_next = this->_head;
    //     for (int i = 0; i < index; i++)
    //     {
    //         insert_prev = insert_prev->next();
    //     }
    //     insert_next = insert_prev->next();

    // }

    void Double_List::append(double data)
{
    DListNode * Node = this->_createNode(data);
    
    if (this->isEmpty())
    {
        this->_head = Node;
        this->_tail = Node;
        
    }else
    {
        (this->_tail)->linkNext(Node);
        Node->linkPrev(this->_tail);
        this->_tail = Node;

    }
}

void Double_List::show() const
{
    if (this->isEmpty())
    {
        cout << "empty list" << endl;
    }
    else
    {
        int i = 0;
        for (const DListNode *temp = this->_head; temp != NULL; temp = temp->next())
        {
            std::cout << i << ": " << temp->data() << std::endl;
            ++i;
        }

    }
}

Double_List:: ~Double_List()
{
    cout <<"clearing"<<endl;
    for (const DListNode *temp = this->_head; temp != NULL; temp = temp->next())
    {
        std::cout << temp->data() << std::endl;
        delete (temp);
        cout << "del!"<<endl;
    }
}