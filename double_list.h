#ifndef __DOUBLE_LIST_H__
#define __DOUBLE_LIST_H__

#include "dlist_node.h"

#include <assert.h>
#include <exception>

#include <stdexcept>

#define NDEBUG


class Double_List
{
    public: 
        Double_List();
        Double_List(double*data, int dataNum);
        ~Double_List();

        void insert(int index, double data);
        void append(double data);

        bool isEmpty() const ;
        void show() const;
        

        double length() const;
        double at(int index) const;

      private: 
        DListNode * _createNode(double data);
        DListNode *_jumpTo(int index) const;
        DListNode *_getCursorToHeader() const;
        
        bool _isIndexOutOfRange(int queryIndex) const throw(std::invalid_argument, int);
        // bool _isIndexOutOfRange(int queryIndex) const;

        DListNode * _tail;
        DListNode * _head;
};



#endif